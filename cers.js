//npm install express sqlite3 ssh2 child_process
const express = require('express');
const sqlite3 = require('sqlite3').verbose();
const { exec } = require('child_process');
const { Client } = require('ssh2');
const fs = require('fs');
const commandsConfig = require('./commands.json');

const app = express();
const port = 3000;

let db = new sqlite3.Database(':memory:', (err) => {
  if (err) {
    console.error(err.message);
  }
  console.log('Connected to the SQLite database.');
});

db.run(`CREATE TABLE command_results (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  command TEXT,
  output TEXT,
  timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
  server TEXT DEFAULT 'local'
)`);

function executeLocalCommands() {
  commandsConfig.localCommands.forEach((command) => {
    exec(command, (err, stdout, stderr) => {
      if (err) {
        console.error(`exec error: ${err}`);
        return;
      }
      storeCommandResult(command, stdout);
    });
  });
}

function executeRemoteCommands() {
  commandsConfig.remoteCommands.forEach((cmdInfo) => {
    const conn = new Client();
    conn.on('ready', () => {
      conn.exec(cmdInfo.command, (err, stream) => {
        if (err) throw err;
        stream.on('close', () => {
          conn.end();
        }).on('data', (data) => {
          storeCommandResult(cmdInfo.command, data.toString(), cmdInfo.host);
        });
      });
    }).connect({
      host: cmdInfo.host,
      port: 22,
      username: cmdInfo.username,
      privateKey: fs.readFileSync(cmdInfo.privateKeyPath)
    });
  });
}

function storeCommandResult(command, output, server = 'local') {
  db.run('INSERT INTO command_results (command, output, server) VALUES (?, ?, ?)', [command, output, server], function(err) {
    if (err) {
      console.error(err.message);
    }
    console.log(`A row has been inserted with rowid ${this.lastID}`);
  });
}

app.get('/results', (req, res) => {
  db.all(`SELECT command, output, timestamp, server
          FROM command_results
          ORDER BY timestamp DESC`, [], (err, rows) => {
    if (err) {
      console.error(err.message);
      res.status(500).send("Error retrieving data");
    } else {
      res.json({ results: rows });
    }
  });
});

setInterval(() => {
  executeLocalCommands();
 // executeRemoteCommands();
}, 60000);

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});
